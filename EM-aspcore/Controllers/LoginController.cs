﻿using Microsoft.AspNetCore.Mvc;

namespace EmAspCore.Controllers
{
    public class LoginController : Controller
    {
        /*
         *  ~/Login　でアクセスする
         * 
         */

        public IActionResult Index()
        {
            return View();
        }

        /*
         * ~/Login/Hello　でアクセスする
         * 
         */
        public string Hello()
        {
            return "とりあえずこれが表示されるならOK？";
        }
    }
}