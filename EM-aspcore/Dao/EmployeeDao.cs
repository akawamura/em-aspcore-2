﻿using EmAspCore.Entity;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace EmAspCore.Dao
{
    public class EmployeeDao : BaseDao<Employee>
    {
        public EmployeeDao(SqlConnection con) : base(con) { }

        public List<Employee> SelectByKey(Employee key)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT * FROM employee");

            // ToDo:ひとまずデバッグ用で全検索
            // ToDo:ここまでデバッグ


            // SQLの実行

            // DBの作りに依存しないようインスタンスに詰め替え
            List<Employee> retList = new List<Employee>();

            return retList;
        }

        public override string getTableName()
        {
            return "employee";
        }

        protected override Employee RowMapping(DataRow row)
        {
            Employee emp = new Employee();
            emp.employeeId = (int)row["id_employee"];
            emp.employeeName = (string)row["nm_employee"];
            emp.employeeKana = (string)row["kn_employee"];
            emp.mailAddress = (string)row["mail_address"];
            emp.password = (string)row["password"];
            emp.departmentId = (int)row["id_department"];
            return emp;
        }
    }
}
