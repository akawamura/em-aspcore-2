﻿using EmAspCore.Entity;
using System.Data;
using System.Data.SqlClient;

namespace EmAspCore.Dao
{
    public class DepartmentDao : BaseDao<Department>
    {
        public DepartmentDao(SqlConnection con) : base(con) { }

        public override string getTableName()
        {
            return "department";
        }

        protected override Department RowMapping(DataRow row)
        {
            Department dep = new Department();
            dep.departmentId = (int)row["id_department"];
            dep.departmentName = (string)row["nm_department"];
            return dep;
        }
    }
}
